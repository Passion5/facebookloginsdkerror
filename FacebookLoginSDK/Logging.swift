//
//  Logging.swift
//  WYD
//
//  Created by Terrill Thorne on 4/21/17.
//  Copyright © 2017 Terrill Thorne. All rights reserved.
//

import Foundation

public class Logging
{
    // This makes app performance consistent
 private var isDebug: Bool
    
  public init()
    {
        self.isDebug = false
    }
 
    public func setup(isDebug: Bool)
    
    {
        
        self.isDebug = isDebug
        print("Project is debug: \(self.isDebug)")
    }
    
    public func VSLog<T>(value: T)
    {
        if self.isDebug == true
        {
            print(value)
        }
    }
    
}
