//
//  ViewController.swift
//  WYD
//
//  Created by Terrill Thorne on 4/14/17.
//  Copyright © 2017 Terrill Thorne. All rights reserved.
//  Have facebook sync information & friends

import UIKit
import Firebase
import FBSDKLoginKit

class ViewController: UIViewController, FBSDKLoginButtonDelegate {

   
    @IBOutlet weak var Email: UITextField!
   
    @IBOutlet weak var Password: UITextField!
    
    
    // LOOK AT USER LOGIN ON YOUTUBE
    // https://www.youtube.com/watch?v=asKXHVUZiIY
    func emailTxtField() {
      // the email has to be authenticated by the internet 
      // if its not then user can't create account
      // add alert field
        
    }
    
    
    func passwordTxtField() {
        // users are required to have seven words with a capital letter & number
        // add alert field
        
        FIRAuth.auth()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Allow user to log in through Facebook
        let loginButton = FBSDKLoginButton()
        
        view.addSubview(loginButton)
        
        loginButton.frame = CGRect(x: 16, y: 380, width: view.frame.width - 32, height: 50)
    
        loginButton.delegate = self
    }
    public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!){
    
    
    /*!
     @abstract Sent to the delegate when the button was used to logout.
     @param loginButton The button that was clicked.
     */
    }
    public func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!){
    
    
    /*!
     @abstract Sent to the delegate when the button is about to login.
     @param loginButton the sender
     @return YES if the login should be allowed to proceed, NO otherwise
     */
    }
 
    // https://www.youtube.com/watch?v=iSszeW1aH6I&t=645s
    @IBAction func Login(_ sender: Any) {
        
        let emailText = Email.text
        let passwordText = Password.text
        
        // Sign the user in
      FIRAuth.auth()?.signIn(withEmail: emailText!, password: passwordText!, completion: { (user, error) in
        
        // Check if user doesn't have account
        if let u = user {
            
        
        // User account found
        } else {
            
        }
      })
    }
    
    @IBAction func SignUp(_ sender: Any) {
        let emailText = Email.text
        let passwordText = Password.text
       
        // Register the user
       FIRAuth.auth()?.createUser(withEmail: emailText!, password: passwordText! , completion: { (user, error) in
       
        // Check if user doesn't have account
        if let u = user {
            // User is found, advance to next screen
            
        } else {
            // Error: User not found, message appears
            
        }
        
       })
    }

            }


